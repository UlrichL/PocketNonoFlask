# PocketNonoFlask

A web application showing a game board based on Flask (2.3) and Python (3.9).

![A screenshot of a web page with the title "Pocket Nono in Pythong" and a number of light green or light red rectangles.](demo.png "Game board display")

## Setup

This app needs Python, Flask and the packages Flask-SQLAlchemy, Flask-WTF and pytest.

## Usage

It can then be started as demo with `flask --app nono run`.

## Current Features

* Template rendering with Jinja2
* Form handling with Flask-WTF
* Store a state in a db with ORM with SQLAlchemy
* Interactivity with JS: click a board tile and change its (backend) state
* Tests for the game state service

## Potential Next Steps

* Allow multiple game states (i. e. session-based)
* Support some actual game play (e. g. compare game state to a desired state)
