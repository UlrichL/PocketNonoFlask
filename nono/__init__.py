import os
import sys

from flask import Flask
from flask_wtf import CSRFProtect

from nono.models import db
from nono.views import GameView, ClickHandler
from nono.services import service_locator, GameStateService


def create_app(test_config=None):
    service_locator.register('game_state_service', GameStateService(db))

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_prefixed_env()  # This loads everything prefixed with FLASK; especially it expects FLASK_SECRET_KEY
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)
    if 'SECRET_KEY' not in app.config or app.config['SECRET_KEY'] is None:
        print('FLASK_SECRET_KEY env variable is not set; using dynamic value; i. e. session is not persistent', file=sys.stderr)
        app.config['SECRET_KEY'] = 'this is not secure ' + str(os.urandom(32))
    # else:
    #     print('There is a secret key in config '+app.config['SECRET_KEY'])

    click_handler_view = ClickHandler.as_view("click")
    app.add_url_rule("/", view_func=GameView.as_view("game"), methods=['GET', 'POST'])
    app.add_url_rule("/click", view_func=click_handler_view, methods=['PATCH'])

    csrf = CSRFProtect()
    csrf.init_app(app)
    csrf.exempt(click_handler_view)

    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"  # memory db; add /file.db for an actual file
    db.init_app(app)
    with app.app_context():
        db.create_all()

    return app
