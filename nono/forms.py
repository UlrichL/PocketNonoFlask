from flask_wtf import FlaskForm
from wtforms import SubmitField, IntegerField
from wtforms.validators import DataRequired, NumberRange


class GameSizeForm(FlaskForm):
    row_count = IntegerField('Rows', validators=[DataRequired(), NumberRange(1, 20)])
    column_count = IntegerField('Columns', validators=[DataRequired(), NumberRange(1, 40)])
    submit = SubmitField('Submit')
