from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class Session(db.Model):
    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String, nullable=False)


class GameStateInDb(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)  # autoincrement: get an id after (first) save
    row_count = db.Column(db.Integer, nullable=False)
    column_count = db.Column(db.Integer, nullable=False)
    fields = db.Column(db.String, nullable=False)
