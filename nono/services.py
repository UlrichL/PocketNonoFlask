import sys

from flask import session
from flask_sqlalchemy import SQLAlchemy

from nono.light_models import GameState
from nono.models import GameStateInDb


# A simple service locator for (singleton) service object retrieval
class ServiceLocator:
    def __init__(self):
        self.__services = {}

    def register(self, name, service_class):
        self.__services[name] = service_class

    def get(self, name):
        # Maybe add some error handling or fallbacks
        return self.__services[name]


service_locator = ServiceLocator()


def log_usage(method):
    def inner(ref, *args):
        print("Used method "+method.__name__)
        return method(ref, *args)
    return inner


class GameStateService:
    def __init__(self, db:SQLAlchemy):
        self.game_state = GameState()
        self.db = db

    def get(self):
        if 'game_id' in session:
            game_state_in_db: GameStateInDb = self.db.session.query(GameStateInDb).filter(GameStateInDb.id == session['game_id']).scalar()
            if game_state_in_db is not None:
                print('Loading game state from session with id ' + str(session['game_id']))
                self.game_state.from_db(game_state_in_db)

        return self.game_state

    @log_usage
    def click(self, row, column):
        self.game_state.click(row, column)
        self.save_state()

    def change_size(self, row_count, column_count):
        self.game_state = self.game_state.copy_with(row_count, column_count)
        self.save_state()

    def save_state(self):
        game_state_for_db = self.game_state.to_db()

        if game_state_for_db.id is None:
            self.db.session.add(game_state_for_db)
        else:
            self.db.session.merge(game_state_for_db)
        self.db.session.commit()

        if game_state_for_db.id is not None:
            self.game_state.db_id = game_state_for_db.id
            if 'game_id' not in session or session['game_id'] != game_state_for_db.id:
                print('Setting game state newly to session with id '+str(game_state_for_db.id))
                session['game_id'] = game_state_for_db.id
        else:
            print('Save error: newly created game state has no id', file=sys.stderr)
