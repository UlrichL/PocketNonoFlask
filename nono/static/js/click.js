var hideTimer = null
document.addEventListener("DOMContentLoaded", () => {
    document.querySelectorAll(".cell").forEach(cell => {
        cell.addEventListener("click", (event) => {
            var xPos = 1, yPos = 1
            var column = event.target.previousElementSibling
            while (column) {
                xPos++
                column = column.previousElementSibling
            }

            var row = event.target.parentNode.previousElementSibling
            while (row) {
                yPos++
                row = row.previousElementSibling
            }

            const xhr = new XMLHttpRequest()

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    document.querySelector(".message").innerText = "Activated fields: "+xhr.responseText
                    document.querySelector(".message").style.display = "block"

                    if (hideTimer !== null) {
                        window.clearTimeout(hideTimer)
                    }

                    hideTimer = window.setTimeout(() => document.querySelector(".message").style.display = "none", 2000)
                }
            }

            xhr.open("PATCH", "/click")
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
            xhr.send("x="+xPos+"&y="+yPos)

            event.target.classList.toggle("active")
        })
    })
})
