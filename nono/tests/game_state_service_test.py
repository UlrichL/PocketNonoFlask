from unittest.mock import Mock

from flask import current_app

from nono import GameStateService, create_app
from nono.light_models import GameState


def test_it_returns_a_game_state():
    db_mock = Mock()
    db_session = Mock()
    db_mock.session = db_session
    db_session.query.return_value = db_session
    db_session.filter.return_value = db_session
    db_session.scalar.return_value = None

    # TODO isn't there a more elegant way to (simply) mock the session object (@mock.patch('flask.session') didn't work)?
    app = create_app()

    with app.app_context():
        with current_app.test_request_context():
            game_state_service = GameStateService(db_mock)
            game_state = game_state_service.get()

            assert isinstance(game_state, GameState)


def test_it_switches_a_field_state():
    db_mock = Mock()
    db_session = Mock()
    db_mock.session = db_session
    db_session.query.return_value = db_session
    db_session.filter.return_value = db_session
    db_session.scalar.return_value = None

    app = create_app()

    with app.app_context():
        with current_app.test_request_context():
            game_state_service = GameStateService(db_mock)
            game_state = game_state_service.get()

            assert game_state.fields[0][0] is False

            game_state_service.click(1, 1)
            game_state = game_state_service.get()

            assert game_state.fields[0][0] is True
