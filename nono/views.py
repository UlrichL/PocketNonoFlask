from flask import render_template, make_response, Response, request
from flask.views import View

from nono.forms import GameSizeForm
from nono.services import service_locator, GameStateService


class GameView(View):
    # This has two functions: Basic page rendering, and it also supports the change size form and acts upon it
    def dispatch_request(self):
        game_state_service: GameStateService = service_locator.get('game_state_service')

        game_state = game_state_service.get()
        form = GameSizeForm(row_count=game_state.row_count, column_count=game_state.column_count)

        if form.validate_on_submit():
            game_state_service.change_size(form.row_count.data, form.column_count.data)
            game_state = game_state_service.get()

        return render_template("game.html", game=game_state, form=form)


class ClickHandler(View):
    def dispatch_request(self):
        line = int(request.form.get('y', 0))
        column = int(request.form.get('x', 0))

        if line == 0 or column == 0:
            return Response(response='missing parameters x or y', status=400, mimetype="text/plain")

        game_state_service: GameStateService = service_locator.get('game_state_service')

        game_state_service.click(line, column)
        game_state = game_state_service.get()

        return Response(response=str(game_state.count_activated_fields()), status=200, mimetype="text/plain")
